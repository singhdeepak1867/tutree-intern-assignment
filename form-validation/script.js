"use-strict";
let emailInp = document.querySelector("#email");
let passInp = document.querySelector("#pwd");
let cpassInp = document.querySelector("#cpass");
let loginBtn = document.querySelector(".form-group_btn");
let emailErrorMsg = document.querySelector(".form-group-error");
let blankErrorMsg = document.querySelector(".blankErrorMsg");
let passDoNotMatch = document.querySelector(".doNotMatch");
let bgOnFocus = document.querySelectorAll(".input-i-container");
let unlockLogo = document.querySelector(".fas fa-unlock");
let envlopeLogo = document.querySelector(".far fa-envelope");
let passwordStrength = document.querySelector(".form-group-strPass");
// taking values
let emailVal = emailInp.value;
let passVal = passInp.value;
let cpassVal = cpassInp.value;
let mailformat;

// attaching eventHandler
loginBtn.addEventListener("click", function (e) {
  // if values are empty
  e.preventDefault();
  if (!emailInp.value || !passInp.value || !cpassInp.value) {
    blankErrorMsg.style.display = "block";
    blankErrorMsg.style.color = "#d63031";
  } else {
    blankErrorMsg.style.display = "none";
  }

  // validating email
  function ValidateEmail(inputText) {
    mailformat =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (inputText.value.match(mailformat)) {
      emailErrorMsg.style.display = "none";
    } else {
      emailErrorMsg.style.display = "block";
      emailErrorMsg.style.color = "#d63031";
    }
  }
  ValidateEmail(emailInp);

  // validating passwords
  function ValidatePassword() {
    if (passInp.value != cpassInp.value) {
      passDoNotMatch.style.display = "block";
      passDoNotMatch.style.color = "#d63031";
      blankErrorMsg.style.display = "none";
      passDoNotMatch.textContent = "Password does not match";
    }
  }
  ValidatePassword();
  //   password length
  if (passInp.value.length < 6 || cpassInp.value.length < 6) {
    passDoNotMatch.style.display = "block";
    passDoNotMatch.style.color = "#d63031";
    passDoNotMatch.innerHTML = "Password should be more than 6 characters";
    if (passInp.value.length == 0 && cpassInp.value.length == 0) {
      passDoNotMatch.style.display = "none";
    }
  }
  // redirecting
  function redirect() {
    // for strong strength
    if (
      email.value.match(mailformat) &&
      passInp.value === cpassInp.value &&
      passwordStrength.innerText == "Strong"
    ) {
      console.log(true);
      window.location.href = "https://google.com";
    }
  }
  // for medium strength
  if (
    email.value.match(mailformat) &&
    passInp.value === cpassInp.value &&
    passwordStrength.innerText == "Medium"
  ) {
    console.log(true);
    window.location.href = "https://google.com";
  }

  redirect();
});
