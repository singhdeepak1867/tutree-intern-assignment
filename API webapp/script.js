"use-strict";

let table = document.querySelector(".table");
let demoLine = document.querySelector(".demo");
const addData = document.querySelector(".addData");
let submitData = document.querySelector(".submit");
let ev;
// demoLine.textContent = "";

// getting data from API
let getData = async function () {
  let response = await fetch("http://localhost:3000/employees");
  let data = await response.json();
  data.forEach((element) => {
    let html = ` <tr class="table">
            <td>${element.id}</td>
            <td>${element.name}</td>
            <td>${element.username}</td>
            <td>${element.email}</td>
            <td>${element.phone}</td>
              <td><button class="edit" onclick="edit()";>EDIT</button></td>
            <td><button class="delete" onclick="del()";>DELETE</button></td>
          </tr>`;

    table.insertAdjacentHTML("beforeend", html);
  });
};
document.onload = getData();

// deleting data
function del() {
  window.addEventListener("click", function (e) {
    let eventDel = e.target.parentNode.parentNode.firstElementChild.innerHTML;
    ev = eventDel;
    console.log(ev, eventDel);
    deleteData(ev);
  });
}

const deleteData = function (ev) {
  let confirmBox = confirm("Are you sure you want to delete?");
  if (confirmBox) {
    const response = fetch(`http://localhost:3000/employees/${ev}`, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json; charset=UTF-8",
        Accept: "application/json",
      },
    });
  }
};

// edit data

function edit() {
  window.addEventListener("click", function (e) {
    let first = e.target.parentNode.parentNode.firstElementChild.innerHTML;
    window.location.href = "./editData.html?id=" + first;
    console.log(first);
  });
}

// function editData(e) {
//   window.location.href = "./editData.html";
// }

// add data
addData.addEventListener("click", function () {
  window.location.href = "./addData.html";
});
