"use-strict";

let submitData2 = document.querySelector(".submit");
// let id2 = document.querySelector(".id-name");
let names2 = document.querySelector(".name");
let username2 = document.querySelector(".username");
let email2 = document.querySelector(".email");
let num = document.querySelector(".phone");
let idErrorMsg = document.querySelector(".id-error-msg");
let nameErrorMsg = document.querySelector(".name-error-msg");
let usernameErrorMsg = document.querySelector(".username-error-msg");
let phoneErrorMsg = document.querySelector(".phone-error-msg");
let emailErrorMsg = document.querySelector(".email-error-msg");

let xyz = new URLSearchParams(window.location.search);
let tempId = xyz.get("id");

let editValue = async function (tempId) {
  console.log(tempId);
  let response = await fetch(`http://localhost:3000/employees/${tempId}`);
  let data = await response.json();
  console.log(data);
  // id2.value = data.id;
  names2.value = data.name;
  username2.value = data.username;
  email2.value = data.email;
  num.value = data.phone;
// id2.classList.add("style")
};

editValue(tempId);

submitData2.addEventListener("click", function (e) {
  e.preventDefault();
  // validating data first
  // if (id2.value == 0) {
  //   idErrorMsg.style.display = "block";
  // }
  if (names2.value.length == 0) {
    nameErrorMsg.style.display = "block";
  }
  if (email2.value.length == 0) {
    emailErrorMsg.style.display = "block";
  }
  if (num.value.length == 0) {
    phoneErrorMsg.style.display = "block";
  }
  if (username2.value.length == 0) {
    usernameErrorMsg.style.display = "block";
  }
  if (num.value.length > 10) {
    phoneErrorMsg.style.display = "block";
    phoneErrorMsg.textContent = "Phone no should be less than 10 characters";
  }
  if (num.value.length > 1 && num.value.length < 10) {
    phoneErrorMsg.style.display = "block";
    phoneErrorMsg.textContent = "Phone no is Invalid";
  }

  function ValidateEmail(inputText) {
    let mailformat =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!inputText.value.match(mailformat) && inputText.value) {
      emailErrorMsg.style.display = "block";
      emailErrorMsg.textContent = "Invalid Email";
    }
  }
  ValidateEmail(email2);
  const editData = async function () {
    let response = await fetch(`http://localhost:3000/employees/${tempId}`, {
      method: "PUT",
      body: JSON.stringify({
        id: `${tempId}`,
        name: `${names2.value}`,
        username: `${username2.value}`,
        email: `${email2.value}`,
        phone: `${num.value}`,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
    let data = response.json();
    console.log(data);
  };
  editData();
  // alert("edit successful");
    window.location.href="./index.html"
  console.log("clicked");
  return false;

});
