"use-strict";

let submitData = document.querySelector(".submit");
let id = document.querySelector(".id-name");
let names = document.querySelector(".name");
let username = document.querySelector(".username");
let email = document.querySelector(".email");
let phone = document.querySelector(".phone");
let idErrorMsg = document.querySelector(".id-error-msg");
let nameErrorMsg = document.querySelector(".name-error-msg");
let usernameErrorMsg = document.querySelector(".username-error-msg");
let phoneErrorMsg = document.querySelector(".phone-error-msg");
let emailErrorMsg = document.querySelector(".email-error-msg");

submitData.addEventListener("click", function (e) {
  e.preventDefault();
  // validating data first
  if (id.value == 0) {
    idErrorMsg.style.display = "block";
  }
  if (names.value == 0) {
    nameErrorMsg.style.display = "block";
  }
  if (email.value == 0) {
    emailErrorMsg.style.display = "block";
  }
  if (phone.value == 0) {
    phoneErrorMsg.style.display = "block";
  }
  if (username.value == 0) {
    usernameErrorMsg.style.display = "block";
  }
  if (phone.value.length > 10) {
    phoneErrorMsg.style.display = "block";
    phoneErrorMsg.textContent = "Phone no should be less than 10 characters";
  }
  if (phone.value.length > 1 && phone.value.length < 10) {
    phoneErrorMsg.style.display = "block";
    phoneErrorMsg.textContent = "Phone no is Invalid";
  }

  function ValidateEmail(inputText) {
    let mailformat =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!inputText.value.match(mailformat) && inputText.value) {
      emailErrorMsg.style.display = "block";
      emailErrorMsg.textContent = "Invalid Email";
    }
  }
  ValidateEmail(email);
  // return false;
  // adding data
  const addData = function (ev) {
    ev.preventDefault();
    fetch("http://localhost:3000/employees", {
      method: "POST",
      body: JSON.stringify({
        id: `${id.value}`,
        name: `${names.value}`,
        username: `${username.value}`,
        email: `${email.value}`,
        phone: `${phone.value}`,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
        Accept: "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => console.log(json));
    alert("Your Data Added successfully");
    // window.location.href = "./index.html";
  };
  submitData.addEventListener("click", addData);

  console.log("clicked");
});
